package util;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.Serializable;

@ManagedBean
@ApplicationScoped
public class AppUrlStore implements Serializable {
    private static final long serialVersionUID = 1L;

    private String baseUrl = null;
    private String clienteCrudUrl = null;
    private String paginaInicial = null;
    private String produtoCrudUrl = null;
    private String fornecedorCrudUrl = null;

    public String getBaseUrl() {return baseUrl; }

    public String getClienteCrudUrl() {
        return clienteCrudUrl;
    }

    public String getPaginaInicial() {
        return paginaInicial;
    }

    public String getProdutoCrudUrl() {
        return produtoCrudUrl;
    }

    public String getFornecedorCrudUrl() {
        return fornecedorCrudUrl;
    }

    @PostConstruct
    public void init() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        String baseUrl = externalContext.getInitParameter("BaseUrl");

        this.baseUrl = baseUrl;
        this.clienteCrudUrl = baseUrl + "cliente.xhtml";
        this.paginaInicial = baseUrl + "index.xhtml";
        this.produtoCrudUrl = baseUrl + "produto.xhtml";
        this.fornecedorCrudUrl = baseUrl + "fornecedor.xhtml";
    }
}

package util;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TndLogger implements Serializable {
    private static final long serialVersionUID = 1L;
    private Logger log;

    private TndLogger(Class<?> clazz) {
        this.log = Logger.getLogger(clazz.getPackage().getName());
    }

    public static TndLogger getLogger(Class<?> clazz) {
        return new TndLogger(clazz);
    }

    public void finer(String msg) {
        this.log.info(msg);
    }

    public void finer(String msg, Object... params) {
        this.log.info(MessageFormat.format(msg, params));
    }

    public void info(String msg) {
        this.log.info(msg);
    }

    public void info(String msg, Object... params) {
        this.log.info(MessageFormat.format(msg, params));
    }

    public void debug(String msg) {
        this.log.fine(msg);
    }

    public void debug(String msg, Object... params) {
        this.log.fine(MessageFormat.format(msg, params));
    }

    public void error(String msg) {
        this.log.severe(msg);
    }

    public void error(String msg, Object... params) {
        this.log.severe(MessageFormat.format(msg, params));
    }

    public void error(String msg, Throwable exception) {
        this.log.log(Level.SEVERE, msg, exception);
    }

    public void error(String msg, Throwable exception, Object... params) {
        this.log.log(Level.SEVERE, MessageFormat.format(msg, params), exception);
    }

    public void warn(String msg) {
        this.log.warning(msg);
    }

    public void warn(String msg, Object... params) {
        this.log.warning(MessageFormat.format(msg, params));
    }
}
package mb;

import entidade.Fornecedor;
import entidade.enums.BooleanEnum;
import util.CommonUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "mbFornecedor")
@SessionScoped
public class MBFornecedor implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<Fornecedor> listaFornecedores;
    private Fornecedor fornecedor = new Fornecedor();
    private Fornecedor fornecedorAntesDaEdicao = null;
    private boolean edicao;
    private BooleanEnum[] tipoRecorrente;


    @ManagedProperty(value = "#{commonUtils}")
    private CommonUtils util;

    public void setUtil(CommonUtils util) {
        this.util = util;
    }

    @PostConstruct
    public void init() {
        listaFornecedores = new ArrayList<Fornecedor>();
    }

    public BooleanEnum[] getTipoRecorrente() {
        return BooleanEnum.values();
    }

    public void adicionar() {

        for(Fornecedor fornecedor : listaFornecedores){
            if(fornecedor.getCnpj().equals(getFornecedor().getCnpj())){
                util.addMessage("Fornecedor Inválido", "Produto com o mesmo nome e fornecedor já existe no banco de dados!");
                return;
            }
        }

        fornecedor.setId(listaFornecedores.isEmpty() ? 1 : listaFornecedores.get(listaFornecedores.size() - 1).getId() + 1);
        listaFornecedores.add(fornecedor);
        fornecedor = new Fornecedor();

        util.redirectWithGet();
    }

    public void resetar() {
        fornecedor = new Fornecedor();

        util.redirectWithGet();
    }

    public void editar(Fornecedor item) {
        fornecedorAntesDaEdicao = item.clonar();
        this.fornecedor = item;
        edicao = true;

        util.redirectWithGet();
    }

    public void cancelar() {
        this.fornecedor.recuperar(fornecedorAntesDaEdicao);
        this.fornecedor = new Fornecedor();
        edicao = false;

        util.redirectWithGet();
    }

    public void gravar() {

        this.fornecedor = new Fornecedor();
        edicao = false;

        util.redirectWithGet();
    }

    public void deletar(Fornecedor item) throws IOException {
        listaFornecedores.remove(item);

        this.fornecedor = new Fornecedor();

        util.redirectWithGet();
    }

    public List<Fornecedor> getListaFornecedores() {
        return listaFornecedores;
    }

    public void setListaFornecedores(List<Fornecedor> listaFornecedores) {
        this.listaFornecedores = listaFornecedores;
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public Fornecedor getFornecedorAntesDaEdicao() {
        return fornecedorAntesDaEdicao;
    }

    public void setFornecedorAntesDaEdicao(Fornecedor fornecedorAntesDaEdicao) {
        this.fornecedorAntesDaEdicao = fornecedorAntesDaEdicao;
    }

    public boolean isEdicao() {
        return edicao;
    }

    public void setEdicao(boolean edicao) {
        this.edicao = edicao;
    }

    public CommonUtils getUtil() {
        return util;
    }
}

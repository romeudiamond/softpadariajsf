package mb;

import entidade.Cliente;
import entidade.enums.CartaoFidelidadeEnum;
import util.CommonUtils;
import util.TndLogger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.util.*;

@ManagedBean(name = "mbCliente")
@SessionScoped
public class MBCliente{
    private static final long serialVersionUID = 1L;

    private static TndLogger log = TndLogger.getLogger(MBCliente.class);

    private List<Cliente> listaCliente;
    private Cliente cliente = new Cliente();
    private Cliente clienteAntesDaEdicao = null;
    private boolean edicao;
    private CartaoFidelidadeEnum[] tipoFidelidade;


    @ManagedProperty(value = "#{commonUtils}")
    private CommonUtils util;

    public void setUtil(CommonUtils util) {
        this.util = util;
    }

    @PostConstruct
    public void init() {
        listaCliente = new ArrayList<Cliente>();
    }


    public CartaoFidelidadeEnum[] getTipoFidelidade() {
        return CartaoFidelidadeEnum.values();
    }

    public void adicionar() {

        for(Cliente clienteLista : listaCliente){
            if(clienteLista.getCpf().equals(cliente.getCpf())){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "CPF Inválido","CPF já existe no banco de dados!"));
                return;
            }
        }

        cliente.setId(listaCliente.isEmpty() ? 1 : listaCliente.get(listaCliente.size() - 1).getId() + 1);
        listaCliente.add(cliente);
        cliente = new Cliente();

        util.redirectWithGet();
    }

    public void resetar() {
        cliente = new Cliente();

        util.redirectWithGet();
    }

    public void editar(Cliente item) {
        clienteAntesDaEdicao = item.clonar();
        this.cliente = item;
        edicao = true;

        util.redirectWithGet();
    }

    public void cancelar() {
        this.cliente.recuperar(clienteAntesDaEdicao);
        this.cliente = new Cliente();
        edicao = false;

        util.redirectWithGet();
    }

    public void gravar() {

        this.cliente = new Cliente();
        edicao = false;

        util.redirectWithGet();
    }

    public void deletar(Cliente item) throws IOException {
        listaCliente.remove(item);

        this.cliente = new Cliente();

        util.redirectWithGet();
    }

    public List<Cliente> getListaCliente() {
        return listaCliente;
    }

    public boolean isEdicao() {
        return edicao;
    }

    public void setEdicao(boolean edicao) {
        this.edicao = edicao;
    }

    public void setListaCliente(List<Cliente> listaCliente) {
        this.listaCliente = listaCliente;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}

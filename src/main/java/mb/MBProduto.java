package mb;

import entidade.Fornecedor;
import entidade.Produto;
import entidade.enums.BooleanEnum;
import util.CommonUtils;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "mbProduto")
@SessionScoped
public class MBProduto {
    private static final long serialVersionUID = 1L;

    private List<Produto> listaProdutos;
    private Produto produto = new Produto();
    private Produto produtoAntesDaEdicao = null;
    private boolean edicao;

    private List<Fornecedor> listaDeFornecedores;
    private Fornecedor fornecedor;

    private BooleanEnum[] perecivel;

    @ManagedProperty(value = "#{commonUtils}")
    private CommonUtils util;

    @ManagedProperty(value = "#{mbFornecedor}")
    private MBFornecedor mbFornecedor;

    public void setUtil(CommonUtils util) {
        this.util = util;
    }

    @PostConstruct
    public void init() {
        listaProdutos = new ArrayList<Produto>();
        listaDeFornecedores = mbFornecedor.getListaFornecedores();
    }

    public BooleanEnum[] getPerecivel() {
        return BooleanEnum.values();
    }


    public void adicionar() {


        try {
            //Validando Fornecedor
            for (Produto produtos : listaProdutos) {
                if (produtos.getNome().equals(produto.getNome()) && produtos.getFornecedor().equals(produto.getFornecedor())) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fornecedor Inválido", "Produto com o mesmo nome e fornecedor já existe no banco de dados!"));
                    return;
                }
            }

            //Populando a lista de Produtos
            produto.setId(listaProdutos.isEmpty() ? 1 : listaProdutos.get(listaProdutos.size() - 1).getId() + 1);
            produto.setFornecedor(fornecedor);
            listaProdutos.add(produto);
            produto = new Produto();

            util.redirectWithGet();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void resetar() {
        produto = new Produto();

        util.redirectWithGet();
    }

    public void editar(Produto item) {
        produtoAntesDaEdicao = item.clonar();
        this.produto = item;
        edicao = true;

        util.redirectWithGet();
    }

    public void cancelar() {
        this.produto.recuperar(produtoAntesDaEdicao);
        this.produto = new Produto();
        edicao = false;

        util.redirectWithGet();
    }

    public void gravar() {

        this.produto = new Produto();
        edicao = false;

        util.redirectWithGet();
    }

    public void deletar(Produto item) throws IOException {
        listaProdutos.remove(item);

        this.produto = new Produto();

        util.redirectWithGet();
    }

    public List<Produto> getListaProdutos() {
        return listaProdutos;
    }

    public void setListaProdutos(List<Produto> listaProdutos) {
        this.listaProdutos = listaProdutos;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Produto getProdutoAntesDaEdicao() {
        return produtoAntesDaEdicao;
    }

    public void setProdutoAntesDaEdicao(Produto produtoAntesDaEdicao) {
        this.produtoAntesDaEdicao = produtoAntesDaEdicao;
    }

    public boolean isEdicao() {
        return edicao;
    }

    public void setEdicao(boolean edicao) {
        this.edicao = edicao;
    }

    public List<Fornecedor> getListaDeFornecedores() {
        return listaDeFornecedores;
    }

    public void setListaDeFornecedores(List<Fornecedor> listaDeFornecedores) {
        this.listaDeFornecedores = listaDeFornecedores;
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public MBFornecedor getMbFornecedor() {
        return mbFornecedor;
    }

    public void setMbFornecedor(MBFornecedor mbFornecedor) {
        this.mbFornecedor = mbFornecedor;
    }
}

package entidade;

import entidade.enums.CartaoFidelidadeEnum;

import java.io.Serializable;

public class Cliente implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String nome, endereco, telefone, cpf;
    private CartaoFidelidadeEnum cartaoFidelidade;

    public Cliente() {

    }

    public Cliente(Long id, String nome, String endereco, String telefone, String cpf, CartaoFidelidadeEnum cartaoFidelidade) {
        this.id = id;
        this.nome = nome;
        this.endereco = endereco;
        this.telefone = telefone;
        this.cpf = cpf;
        this.cartaoFidelidade = cartaoFidelidade;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public CartaoFidelidadeEnum getCartaoFidelidade() {
        return cartaoFidelidade;
    }

    public void setCartaoFidelidade(CartaoFidelidadeEnum cartaoFidelidade) {
        this.cartaoFidelidade = cartaoFidelidade;
    }

    public Cliente clonar(){
        return new Cliente(id, nome, endereco, telefone, cpf, cartaoFidelidade);
    }

    public void recuperar(Cliente cliente){
        this.id = cliente.getId();
        this.nome = cliente.getNome();
        this.endereco = cliente.getEndereco();
        this.telefone = cliente.getTelefone();
        this.cpf = cliente.getCpf();
        this.cartaoFidelidade = cliente.getCartaoFidelidade();
    }
}

package entidade;

import entidade.enums.BooleanEnum;
import interfaces.SampleEntity;

import java.io.Serializable;

public class Produto implements Serializable, SampleEntity {
    private static final long serialVersionUID = 1L;

    private String nome;
    private Fornecedor fornecedor;
    private double precoCusto, precoFinal;
    private BooleanEnum perecivel;
    private Long id, unidade;

    public Produto(){

    }

    public Produto(String nome, Fornecedor fornecedor, double precoCusto, double precoFinal, BooleanEnum perecivel, Long id, Long unidade) {
        this.nome = nome;
        this.fornecedor = fornecedor;
        this.precoCusto = precoCusto;
        this.precoFinal = precoFinal;
        this.perecivel = perecivel;
        this.id = id;
        this.unidade = unidade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public double getPrecoCusto() {
        return precoCusto;
    }

    public void setPrecoCusto(double precoCusto) {
        this.precoCusto = precoCusto;
    }

    public double getPrecoFinal() {
        return precoFinal;
    }

    public void setPrecoFinal(double precoFinal) {
        this.precoFinal = precoFinal;
    }

    public BooleanEnum getPerecivel() {
        return perecivel;
    }

    public void setPerecivel(BooleanEnum perecivel) {
        this.perecivel = perecivel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUnidade() {
        return unidade;
    }

    public void setUnidade(Long unidade) {
        this.unidade = unidade;
    }

    public Produto clonar() {
        return new Produto(nome, fornecedor, precoCusto, precoFinal, perecivel, id, unidade);
    }

    public void recuperar(Produto produto){
        this.id = produto.getId();
        this.nome = produto.getNome();
        this.fornecedor = produto.getFornecedor();
        this.perecivel = produto.getPerecivel();
        this.precoCusto = produto.getPrecoCusto();
        this.precoFinal = produto.getPrecoFinal();
        this.unidade = produto.getUnidade();
    }
}

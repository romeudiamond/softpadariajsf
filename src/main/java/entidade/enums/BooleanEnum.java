package entidade.enums;

public enum BooleanEnum {
    SIM(0, "Sim"),
    NÃO(1,"Não");

    private int valor;
    private String label;

    BooleanEnum(int valor, String label) {
        this.valor = valor;
        this.label = label;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}

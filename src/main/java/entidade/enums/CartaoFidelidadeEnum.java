package entidade.enums;

public enum CartaoFidelidadeEnum {

    SILVER(0, "Silver"),
    GOLD(1,"Gold"),
    PREMIUM(2,"Premium");

    private int valor;
    private String label;

    CartaoFidelidadeEnum(int valor, String label) {
        this.valor = valor;
        this.label = label;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}

package entidade;

import entidade.enums.BooleanEnum;
import interfaces.SampleEntity;

import java.io.Serializable;

public class Fornecedor implements Serializable, SampleEntity {
    private static final long serialVersionUID = 1L;

    private String nome, endereco, cnpj;
    private BooleanEnum recorrente;
    private Long id;

    public Fornecedor() {}

    public Fornecedor(String nome, String endereco, String cnpj, BooleanEnum recorrente, Long id) {
        this.nome = nome;
        this.endereco = endereco;
        this.cnpj = cnpj;
        this.recorrente = recorrente;
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public BooleanEnum getRecorrente() {
        return recorrente;
    }

    public void setRecorrente(BooleanEnum recorrente) {
        this.recorrente = recorrente;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Fornecedor clonar() {
        return new Fornecedor(nome, endereco, cnpj, recorrente, id);
    }

    public void recuperar(Fornecedor fornecedor){
        this.id = fornecedor.getId();
        this.nome = fornecedor.getNome();
        this.cnpj = fornecedor.getCnpj();
        this.endereco = fornecedor.getEndereco();
        this.recorrente = fornecedor.getRecorrente();
    }
}
